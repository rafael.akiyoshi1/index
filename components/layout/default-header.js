import React from 'react';
import Layout from 'antd/lib/layout';
import Menu from 'antd/lib/menu';
import { Link, Router } from 'ff-app-routes';

const { Header } = Layout;

const styles = {
  menu: {
    lineHeight: '64px'
  },
  logo: {
    fontWeight: 'bold',
    color: '#fff'
  }
};
// console.log(Rt.routeChangeStart)
const DefaultHeader = () => (
  <Header>
    <Menu mode="horizontal" theme="dark" style={styles.menu}>
      <Menu.Item>
        <Link href="/">
          <h1 style={styles.logo}>Oh My Code</h1>
        </Link>
      </Menu.Item>
      <Menu.Item>
        <Link route="about" params={{ id: 'hello-world' }}>
          <h1>About</h1>
        </Link>
      </Menu.Item>
    </Menu>
  </Header>
);

export default DefaultHeader;
