import React, { PureComponent } from 'react';
import Signin from '../signin';
import './style.less';

class Landing extends PureComponent {
  render() {
    return (
      <div className="landing-container">
        <div className="my-container">
          <div className="login-slogan">
            <h1>Cleaning</h1>
            <h5>fluency as a service</h5>
          </div>
          <Signin {...this.props} />
        </div>
      </div>
    );
  }
}

export default Landing;
