const webpack = require('webpack'); // eslint-disable-line import/no-extraneous-dependencies
const withFonts = require('next-fonts');

/* eslint-disable */
const withLess = require('@zeit/next-less')
const lessToJS = require('less-vars-to-js')
const fs = require('fs')
const path = require('path')
const {
    WebpackBundleSizeAnalyzerPlugin
} = require('webpack-bundle-size-analyzer')

require('dotenv').config();

const { ANALYZE } = process.env

// Where your antd-custom.less file lives
const themeVariables = lessToJS(
    fs.readFileSync(path.resolve(__dirname, './assets/theme/antd-custom.less'), 'utf8')
)

// fix: prevents error when .less files are required by node
if (typeof require !== 'undefined') {
    require.extensions['.less'] = file => {}
}

module.exports = withFonts(withLess({
    enableSvg: true,
    distDir: '.client-dist',
    webpack: (config) => {
        const env = Object.keys(process.env).reduce((acc, curr) => {
            acc[`process.env.${curr}`] = JSON.stringify(process.env[curr]);
            return acc;
        }, {});

        config.plugins.push(new webpack.DefinePlugin(env));
        if (ANALYZE) {
            config.plugins.push(new WebpackBundleSizeAnalyzerPlugin('stats.txt'))
        }
        // alias
        config.resolve.alias['ff-components'] = path.join(__dirname, 'components');
        config.resolve.alias['ff-rematch'] = path.join(__dirname, 'rematch');
        config.resolve.alias['ff-app-routes'] = path.join(__dirname, 'server/routes');
        config.resolve.alias['ff-helpers'] = path.join(__dirname, 'helpers');

        return config;
    },
    useFileSystemPublicRoutes: false,
    lessLoaderOptions: {
        javascriptEnabled: true,
        modifyVars: themeVariables // make your antd custom effective
    }
}));


