const express = require('express');
const nextI18NextMiddleware = require('next-i18next/middleware');
const nextI18next = require('../helpers').nextI18next;
const helmet = require('helmet');
const IS_PROD = require('../config');
var exports = (module.exports = {});
var compression = require('compression');
var bodyParser = require('body-parser');

exports.ServerInit = staticPath => {
  const server = express();
  server.use(compression());
  server.use(helmet(), express.static(staticPath, { maxage: !IS_PROD ? '0' : '1d' }));
  server.use(bodyParser.urlencoded({ extended: false }));

  // parse application/json
  server.use(bodyParser.json());
  server.use(nextI18NextMiddleware(nextI18next));

  return server;
};
