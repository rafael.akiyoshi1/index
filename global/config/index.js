const os = require('os')
require('dotenv').config({ path: require('path').resolve(__dirname, '../.env') }); // eslint-disable-line global-require
var exports = module.exports = {};
const NODE_ENV = process.env.NODE_ENV ? process.env.NODE_ENV.toLowerCase() : 'development';
exports.NODE_ENV = NODE_ENV;
exports.IS_DEV = NODE_ENV === 'development';
exports.IS_PROD = NODE_ENV === 'production';
exports.IS_TEST = NODE_ENV === 'test';