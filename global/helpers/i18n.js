
// const { localeSubpaths } = require('next/config').default().publicRuntimeConfig
const NextI18Next = require('next-i18next/dist/commonjs')
var exports = module.exports = {};

exports.nextI18next = new NextI18Next({
  otherLanguages: ['pt', 'en'],

})
