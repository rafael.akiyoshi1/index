const APP_ROUTES = [
  { name: 'index', pattern: '/', page: 'landing/index' },
];

module.exports = APP_ROUTES;
