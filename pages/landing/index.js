import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { i18n, withNamespaces } from 'ff-helpers/nextI18n';
import { Button, Select } from "antd";

const Option = Select.Option;

class Landing extends PureComponent {

    static async getInitialProps() {
      return {
        namespacesRequired: ['landing']
      }
    }

  handleChange = value => {
    i18n.changeLanguage(value);
  };


    render() {
      const { t } = this.props;
      return (
        <div>
          <Select
            defaultValue={i18n.language}
            style={{ width: 120 }}
            onChange={this.handleChange}
            >
            <Option id="english" value="en">
              English
              </Option>
            <Option id="portuguese" value="pt">
              Portuguese
              </Option>
          </Select>
          <br/>
          <p>
            {t('title')}
          </p>
        </div>
      );
    }
}

Landing.propTypes = {
  t: PropTypes.func.isRequired,
}

export default withNamespaces('landing')(Landing);
