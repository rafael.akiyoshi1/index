import { init } from '@rematch/core';
import counter from '../pages/models';

// rematch store with initialValue set to 5
export default (initialState = {}) => {
  return init({
    models: {
      counter
    },
    redux: {
      initialState
    }
  });
};
