export const { IS_DEV, IS_TEST, NODE_ENV, IS_PROD, moleculerConfig } = require('../global/config');

export const PORT = process.env.PORT ? parseInt(process.env.PORT, 10) : 5000;
