import next from 'next';
import path from 'path';
import { PORT, IS_PROD } from './config';
import { logger } from './logger';
import appRoutes from './routes';



const ServerInit = require('../global/server-conf').ServerInit;

const app = next({
  dev: !IS_PROD,
  dir: path.join(__dirname, '../')
});
const handle = appRoutes.getRequestHandler(app);

app.prepare().then(() => {
  const staticPath = path.join(__dirname, '../static');
  const server = ServerInit(staticPath);

  server.get('*', handle);
    server.listen(PORT, err => {
    if (err) throw err;
    logger.info(`> Ready on http://localhost:${PORT}`);
  });
});
