import NextI18Next from "next-i18next";

const nextI18next = new NextI18Next({
  otherLanguages: ["pt", "en"],
  backend: {
    loadPath: "/static/locales/{{lng}}/{{ns}}.json"
  }
});

const { appWithTranslation, withNamespaces, i18n } = nextI18next;

module.exports = {
  appWithTranslation,
  withNamespaces,
  i18n
};
